import '../models/movie.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

final String base = 'http://www.omdbapi.com/';
final String apikey = '7a705976';

class OMDB {
  static Future<List<Movie>> getMovies({ String q, int y: 2018 }) async {
    http.Response response = await http.get('$base?s=$q&y=$y&apikey=$apikey');

    if (response.statusCode == 200) {
      Map<String, dynamic> body = json.decode(response.body);
      List<Movie> movies = [];
      List search = body["Search"];
      search.forEach((item) {
        movies.add(new Movie(
            title: item["Title"],
            year: item["Year"],
            imdbID: item["imdbID"],
            type: item["Type"],
            poster: item["Poster"]
        ));
      });
      return movies;
    } else {
      throw('An error occurred');
    }
  }

  static Future<Movie> getMovie({ String id }) async {
    http.Response response = await http.get('$base?i=$id&apikey=$apikey');

    if (response.statusCode == 200) {
      Map<String, dynamic> body = json.decode(response.body);
      Movie movie = new Movie(
        title: body["Title"],
        year: body["Year"],
        imdbID: body["imdbID"],
        type: body["Type"],
        poster: body["Poster"],

        rated: body["Rated"],
        released: body["Released"],
        runtime: body["Runtime"],
        genre: body["Genre"],
        director: body["Director"],
        writer: body["Writer"],
        actors: body["Actors"],
        plot: body["Plot"],
        language: body["Language"],
        country: body["Country"],
        awards: body["Awards"],
        imdbRating: body["imdbRating"],
        imdbVotes: body["imdbVotes"],
        production: body["Production"],
        website: body["Website"]
      );
      return movie;
    } else {
      throw('An error occurred');
    }
  }
}
