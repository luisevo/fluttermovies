import 'package:flutter/material.dart';
import '../widgets/movie-item.dart';
import '../models/movie.dart';
import '../services/omdb.dart';

class DetailPage extends StatefulWidget {
  final String title;
  final String id;

  DetailPage({this.title, this.id});

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<DetailPage> {
  Movie movie;

  TextStyle _styleTitle = TextStyle(fontSize: 25.0);
  TextStyle _styleBody = TextStyle(fontSize: 20.0);

  @override
  void initState() {
    super.initState();
    OMDB.getMovie(id: this.widget.id)
    .then((Movie movie) {
      setState(() {
        this.movie = movie;
      });
    });
  }

  Widget _separator() => SizedBox(height: 15.0);

  Widget buildList() {
    return ListView(
      children: <Widget>[
        (this.movie.poster != "N/A") ? Image.network(this.movie.poster, height: 200.0, fit: BoxFit.cover,) : SizedBox(height: 0,),

        Text(this.movie.title, style: _styleTitle),
        Text(this.movie.genre, style: TextStyle(fontSize: 15.0),),
        _separator(),
        Text(this.movie.plot, style: _styleBody, textAlign: TextAlign.justify),

        _separator(),
        Text('Director', style: _styleTitle, textAlign: TextAlign.justify),
        Divider(),
        Text(this.movie.director, style: _styleBody, textAlign: TextAlign.justify),

        _separator(),
        Text('Escritores', style: _styleTitle, textAlign: TextAlign.justify),
        Divider(),
        Text(this.movie.writer, style: _styleBody, textAlign: TextAlign.justify),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(this.widget.title),
        ),
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
          child: (this.movie != null) ?  this.buildList() : CircularProgressIndicator(),
        )
    );
  }
}


