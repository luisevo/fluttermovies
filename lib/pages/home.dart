import 'package:flutter/material.dart';
import './detail.dart';
import '../widgets/movie-item.dart';
import '../models/movie.dart';
import '../services/omdb.dart';

class HomePage extends StatefulWidget {
  final String title;

  HomePage({this.title});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Movie> movies = [];

  @override
  void initState() {
    super.initState();
    this.search();
  }

  void search({ String q = 'marvel' }) {
    OMDB.getMovies(q: q)
        .then((List<Movie> movies) {
      setState(() {
        this.movies = movies;
      });
    })
        .catchError((error) => print(error));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              TextField(
                decoration: InputDecoration(hintText: 'Search'),
                onSubmitted: (String value) {
                  this.search(q: value);
                },
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: this.movies.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) {
                                return DetailPage(
                                    id: this.movies[index].imdbID,
                                    title: this.movies[index].title
                                );
                            })
                        );
                      },
                      child: MovieItem(this.movies[index]),
                    );
                  },
                ),
              )
            ],
          ),
        )
    );
  }
}


