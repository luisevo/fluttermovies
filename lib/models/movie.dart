class Movie {
  String title;
  String year;
  String imdbID;
  String type;
  String poster;

  String rated;
  String released;
  String runtime;
  String genre;
  String director;
  String writer;
  String actors;
  String plot;
  String language;
  String country;
  String awards;
  String imdbRating;
  String imdbVotes;
  String production;
  String website;

  Movie({
    this.title,
    this.year,
    this.imdbID,
    this.type,
    this.poster,

    this.rated,
    this.released,
    this.runtime,
    this.genre,
    this.director,
    this.writer,
    this.actors,
    this.plot,
    this.language,
    this.country,
    this.awards,
    this.imdbRating,
    this.imdbVotes,
    this.production,
    this.website,
  });


}