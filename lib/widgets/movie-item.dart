import 'package:flutter/material.dart';
import 'package:flutter_movies/models/movie.dart';

class MovieItem extends StatelessWidget {
  Movie movie;
  MovieItem(this.movie);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(top: 20.0),
      child: Column(
        children: <Widget>[
          Padding(padding: EdgeInsets.all(10.0),),
          Text(movie.title, style: TextStyle(fontSize: 30.0), textAlign: TextAlign.center,),
          Text(movie.year, style: TextStyle(fontSize: 20.0), textAlign: TextAlign.center,),
          Padding(padding: EdgeInsets.all(5.0),),
          (movie.poster != "N/A") ? Image.network(movie.poster) : SizedBox(height: 0.0,),
          Padding(padding: EdgeInsets.all(10.0),),
        ],
      ),
    );
  }
}